<?php
/*
Template Name: Product - No Sidebar
Template Post Type: products
*/
get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row">
	<header role="page-header">
		<h2 class="text-center"><?php the_title(); ?></h2>
		<ul class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></ul>
	</header>
	<section class="clearfix">
		<aside class="product_content column large-12">
			<?php the_content(); ?>

				<?php if(have_rows('product_content')): ?>
				<?php while(have_rows('product_content')) : the_row(); ?>

					<?php if(get_row_layout() == 'store_availability'): ?>
	          <article class="cleafix store_availability">
							<h4>Available In-Store and On-Display</h4>
		          <div class="isp">
			          <?php if( have_rows('available_store_products')): ?>
			          <?php while(has_sub_field('available_store_products')): ?>
			            <?php $image = get_sub_field('asp_imagery');
			             if( !empty($image) ): ?>
			             <div class="fpc clearfix">
			               <div class="product" style="background: url('<?php echo $image['url']; ?>') no-repeat center center; background-size: contain;"></div>
			               <?php if(get_sub_field('asp_caption')): ?>
			                 <p><?php the_sub_field('asp_caption'); ?></p>
			               <?php endif; ?>
			             </div>
			          <?php endif; ?>
			          <?php endwhile; ?>
		          </div>
	          </article>
	          <?php endif; ?>
	        <?php endif; ?>

					<?php if(get_row_layout() == 'content_block'): ?>
						<article class="clearfix pContent">
							<?php if( have_rows ('product_cb')): ?>
								<?php while(has_sub_field('product_cb')): ?>
									<?php if(get_sub_field('content')): ?>
										<p><?php the_sub_field('content'); ?></p>
									<?php endif; ?>
								<?php endwhile; ?>
							<?php endif; ?>
						</article>
					<?php endif; ?>

					<?php if(get_row_layout() == 'major_product_brands'): ?>
						<article class="clearfix mb">
							<?php if( have_rows ('mpb_overview') ): ?>
								<?php while( has_sub_field('mpb_overview') ): ?>
									<h4>Major Brands</h4>
									<?php if(get_sub_field('mpb_paragraph')): ?>
										<p><?php the_sub_field('mpb_paragraph'); ?></p>
									<?php endif; ?>
								<?php endwhile; ?>
							<?php endif; ?>
							<?php if( have_rows ('mpb_logos')): ?>
								<div class="brands">
									<?php while(has_sub_field('mpb_logos')): ?>
										<?php $image = get_sub_field('mpb_logo');
										 if( !empty($image) ): ?>
										 <div class="brand_logo float-left small-12 medium-4 large-4" style="background: url('<?php echo $image['url']; ?>') no-repeat center center; background-size: contain;"></div>
										<?php endif; ?>
									<?php endwhile; ?>
								</div>
							<?php endif; ?>
						</article>
					<?php endif; ?>

					<?php if(get_row_layout() == 'rpfy_block'): ?>
						<article class="clearfix rbfy">
							<?php if( have_rows ('rpfy_fg')): ?>
								<?php while(has_sub_field('rpfy_fg')): ?>

									<?php if(get_sub_field('rpfy_wyswig')): ?>
										<div class="column medium-7">
											<p><?php the_sub_field('rpfy_wyswig'); ?></p>
										</div>
									<?php endif; ?>
									<?php
									$image = get_sub_field('rpfy_imagery');
									if( !empty($image) ): ?>
										<div class="column medium-5">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
										</div>
									<?php endif; ?>

								<?php endwhile; ?>
							<?php endif; ?>
						</article>
					<?php endif; ?>

			<?php endwhile;?>
			<?php endif;?>
		</aside>
	</section>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
