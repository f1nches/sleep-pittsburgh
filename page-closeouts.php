<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="row">
            <header role="page-header">
                <h2 class="text-center">Closeout Deals</h2>
            </header>
            <section class="clearfix closeout-page">
                <aside class="column large-12">
                    <div class="closeout-deal-text">
                        <?= get_field('promo_text') ?>
                    </div>
                    <div class="closeouts clearfix">
                        <?php
// check if the repeater field has rows of data
                        if (have_rows('deals')):

                            // loop through the rows of data
                            while (have_rows('deals')) : the_row();
                                ?>
                                <div class="medium-4 columns">
                                    <div class="closeout-entry">
                                        <div class="product" style="max-height: none;">
                                            <?php if (get_sub_field('closeout_off')): ?>
                                                <span class="label off">$<?= get_sub_field('closeout_off') ?><br>off</span>
                                            <?php endif; ?>
                                            <img src="<?= get_sub_field('image')['url'] ?>" alt="<?= get_sub_field('name') ?> Closeouts in Pittsburgh">
                                        </div>
                                        <div class="desc-area">
                                            <h3><?= get_sub_field('name') ?></h3>
                                            <ul>
                                                <?php while (have_rows('description')) : the_row(); ?>
                                                    <li><?= get_sub_field('point') ?></li>
                                                <?php endwhile; ?>
                                            </ul>
                                        </div>
                                        <div class="costs">
                                            <h4>
                                                <small>Was $<?= get_sub_field('was_price') ?></small><br>
                                                Now $<?= get_sub_field('now_price') ?>
                                            </h4>
                                        </div>
                                    </div>

                                </div>
                                <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                    <div class="closeout-avail-text">
                        <?= get_field('availability_text'); ?>
                    </div>
                </aside>
            </section>
        </div>
        <?php
    endwhile;
endif;
?>
<script>

    (function ($) {
        function readjust_cells() {
            var height = 0, imgheight = 0;

            $('.desc-area').each(function (ind, ele) {
                var $ele = $(ele);
                $ele.css('height', '');
                if ($(ele).height() > height) {
                    height = $(ele).height();
                }

                $ele.data('original-height', height);
            });

            $('.desc-area').height(height);

            $('.product').each(function (ind, ele) {
                var $ele = $(ele);
                $ele.css('height', '');
                if ($(ele).height() > imgheight) {
                    imgheight = $(ele).height();
                }

                $ele.data('original-height', imgheight);
            });

            console.log('height', imgheight);

            $('.product').height(imgheight);
        }
        ;

        $(document).ready(function () {
            readjust_cells();
        });

        var a;
        $(window).on('resize', function () {
            clearTimeout(a);
            a = setTimeout(readjust_cells, 250);
        });

    }(jQuery));

</script>
<?php get_footer(); ?>
