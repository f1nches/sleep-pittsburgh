$(function() {

	$('.mattress-block:not(:first-child)').hide();

	var default_headline = "Ready to shop for a new mattress?";
	var default_paragraph = "The best way to start is by narrowing your search based on the mattress sizes below, your space, and your budget. Then visit one of our two Pittsburgh area showrooms in either Homestead or Shaler Village so you can try out different mattress options and get a better night's sleep.";

	$("#twin_trigger").click(function(e) {
		e.preventDefault();
		if($(this).children().hasClass("active")) {
			$(this).children().removeClass('active');
			$("#tst").removeClass('active');
			$("#dmh").html(default_headline);
			$("#dmd").html(default_paragraph);
		} else {
			$(this).children().addClass('active');
			$('#dmh').html($('#tmh').html());
			$('#dmd').html($('#tmd').html());
			$("#tst").addClass('active');
			$("#fst, #qst, #kst").removeClass('active');
			$("#full_trigger, #queen_trigger, #king_trigger").children().removeClass('active');
		}
	});
	$("#full_trigger").click(function(e) {
		e.preventDefault();
		if($(this).children().hasClass("active")) {
			$(this).children().removeClass('active');
			$("#fst").removeClass('active');
			$("#dmh").html(default_headline);
			$("#dmd").html(default_paragraph);
		} else {
			$(this).children().addClass('active');
			$('#dmh').html($('#fmh').html());
			$('#dmd').html($('#fmd').html());
			$("#fst").addClass('active');
			$("#tst, #qst, #kst").removeClass('active');
			$("#twin_trigger, #queen_trigger, #king_trigger").children().removeClass('active');
		}
	});
	$("#queen_trigger").click(function(e) {
		e.preventDefault();
		if($(this).children().hasClass("active")) {
			$(this).children().removeClass('active');
			$("#qst").removeClass('active');
			$("#dmh").html(default_headline);
			$("#dmd").html(default_paragraph);
		} else {
			$(this).children().addClass('active');
			$('#dmh').html($('#qmh').html());
			$('#dmd').html($('#qmd').html());
			$("#qst").addClass('active');
			$("#tst, #fst, #kst").removeClass('active');
			$("#twin_trigger, #full_trigger, #king_trigger").children().removeClass('active');
		}
	});
	$("#king_trigger").click(function(e) {
		e.preventDefault();
		if($(this).children().hasClass("active")) {
			$(this).children().removeClass('active');
			$("#kst").removeClass('active');
			$("#dmh").html(default_headline);
			$("#dmd").html(default_paragraph);
		} else {
			$(this).children().addClass('active');
			$('#dmh').html($('#kmh').html());
			$('#dmd').html($('#kmd').html());
			$("#kst").addClass('active');
			$("#tst, #fst, #qst").removeClass('active');
			$("#twin_trigger, #full_trigger, #queen_trigger").children().removeClass('active');
		}
	});
});
