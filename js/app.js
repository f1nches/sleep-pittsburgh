/* Match height for our commitment aside(s) */
$(".our_commitment aside").matchHeight();

/* Hamburger Trigger: Click Function */
$("#hamburger").click(function(e) {
	e.preventDefault();
	$(this).toggleClass('transform');
	$(".primary_nav").toggleClass('show');
	$(".main_nav").slideToggle().toggleClass('show');
	var visible = $(".main_nav").is(".show");
	if (visible === true) {
		$('.main_nav').fadeIn().addClass("show");
	} else {
		$('.main_nav').fadeOut().removeClass("show");
	}
});

$("#trigger").click(function(e) {
	e.preventDefault();
	$(".msidebar li:not(:first-child)").slideToggle();
});

$(document).ready(function() {
  $(".owl-carousel").owlCarousel({
    items: 1,
    dots: true,
  });
});
