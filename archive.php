<?php get_header(); ?>
<div class="row">
	<header role="page-header">
		<h2 class="text-center">Archives</h2>
	</header>
	<section class="clearfix">
		<aside class="column large-12">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'entry' ); ?>
		<?php endwhile; endif; ?>
		</aside>
	</section>
</div>
<?php get_footer(); ?>
