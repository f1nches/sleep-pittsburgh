<section class="entry-summary">
<?php the_excerpt(); ?>
<?php if( is_search() ) { ?><div class="entry-links"><?php wp_link_pages(); ?></div><?php } ?>
<a href="<?php the_permalink(); ?>" class="read-more">Continue Reading <i>&rarr;</i></a>
</section>
