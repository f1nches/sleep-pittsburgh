<?php
/* Template Name: Contact Us */
get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row">
	<header role="page-header">
		<h2 class="text-center"><?php the_title(); ?></h2>
		<ul class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></ul>
	</header>
</div>
<section class="clearfix contact_location">
	<aside class="c_location">
		<h4><?php the_field('name', 'options'); ?></h4>
		<p><?php the_field('address', 'options'); ?></p>
		<p><strong>Phone:</strong> <a href="tel:+1-<?php the_field('phone_number', 'options'); ?>" style="color: white;"><?php the_field('phone_number', 'options'); ?></a></p>
		<ul class="store_hours">
			<?php while(has_sub_field('store_hours', 'option')): ?>
				<li>
					<strong><?php the_sub_field('day'); ?>: </strong>
					<?php the_sub_field('hours_open'); ?>
				</li>
			<?php endwhile; ?>
		</ul>
	</aside>
	<aside class="gmap">
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZaYdFcbQbQYZV7kaiLKX28tOz_8-LtcM"></script>
		<script type="text/javascript">
		window.onload = function() {
			(function($) {
			/*************************************
				Render map in jQuery element
			*************************************/
			function new_map( $el ) {
				// Marker variable
				var $markers = $el.find('.marker');
				// Marker variable argument(s)
				var args = {
					zoom: 16,
					center: new google.maps.LatLng(0, 0),
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					scrollwheel:  false,
					disableDefaultUI: true,
					disableDoubleClickZoom: true
				};
				// Create google map
				var map = new google.maps.Map( $el[0], args);
				// Add marker reference
				map.markers = [];
				// Add marker to map
				$markers.each(function(){
		    	add_marker( $(this), map );
				});
				// Center google map
				center_map(map);
				// Return map
				return map;
			}
			/*************************************
				Adds Marker to Google Map
			*************************************/
			function add_marker( $marker, map ) {
				// Latitude & Longitude Variable(s)
				var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
				// Create Marker
				var marker = new google.maps.Marker({
					position	: latlng,
					map			: map,
	  			icon: {
						url: "<?php echo get_template_directory_uri(); ?>/images/icons/blue-star.svg",
					}
				});
				// Add marker to array
				map.markers.push( marker );
				// If marker has HTML, add to info window.
				if($marker.html()) {
					// create info window
					var infowindow = new google.maps.InfoWindow({
						content: $marker.html()
					});
					// show info window when marker is clicked
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open( map, marker );
					});
				}
			}
			/*************************************
				Center map and show marker
			*************************************/
			function center_map( map ) {
				// Longitude & Latitude Bounds Variable
				var bounds = new google.maps.LatLngBounds();
				// loop through all markers and create bounds
				$.each(map.markers, function(i, marker) {
					var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
					bounds.extend( latlng );
				});
				// Only 1 Marker [Centered]
				if(map.markers.length == 1){
			    map.setCenter( bounds.getCenter() );
			    map.setZoom( 14 );
				}
				// Fit to map container bounds
				else {
					map.fitBounds( bounds );
				}
			}
			/*************************************
				Renders new map on document.ready
			*************************************/
			// Map null variable
			var map = null;
			$(document).ready(function(){
				$('.acf-map').each(function(){
					map = new_map($(this));
				});
			});
			})(jQuery);
		}
		</script>
		<?php $location = get_field('google_map_location');
		if( !empty($location)): ?>
		<div class="acf-map">
			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"><p>
				<strong>Sleep Pittsburgh</strong><br />
				401 East 8th Avenue
				Homestead, PA 15120
			</p></div>
		</div>
		<?php endif; ?>
	</aside>
</section>
<section class="contact_us">
	<div class="row">
		<article class="column text-center">
			<?php echo do_shortcode('[gravityform id=1 title=false description=true ajax=true]'); ?>
			<hr>
		</article>
	</div>
</section>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
