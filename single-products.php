<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="row">
            <header class="pws_header" role="page-header">
                <h2 class="text-center"><?php the_title(); ?></h2>
                <ul class="breadcrumbs"><?php
                    if (function_exists('bcn_display')) {
                        bcn_display();
                    }
                    ?></ul>
            </header>
            <section class="clearfix">
                <aside class="product_sidebar column large-3">
                    <ul>
                        <?php
                        $menu = wp_get_nav_menu_items('Primary Nav');
                        if(empty($post->post_parent)) {
                            $post_ID = get_the_ID();
                        } else {
                            $post_ID = $post->post_parent;
                        }
                        
                        echo "<ul id='sub-nav'>";
                        foreach ($menu as $item) {
                            if ($post_ID == $item->object_id) {
                                $menu_parent = $item->ID;
                            }
                            if (isset($menu_parent) && $item->menu_item_parent == $menu_parent) {
                                echo "<li><a href='" . $item->url . "'>" . $item->title . "</a></li>";
                            }
                        }
                        echo "</ul>";
                        ?>
                    </ul>
                    <!--
                    <div style="max-width: 200px; padding-top: 20px;">
        <a href="http://www.sleeppittsburgh.com/specials/">
        <img src="http://www.sleeppittsburgh.com/wp-content/uploads/2017/08/SIDEBAR-DEALS-01-e1502207091346.png" alt="Great Mattress Deals in Pittsburgh">
        </a>
        </div>
                    -->
                </aside>
        <?php echo do_shortcode('<ul class="column msidebar"><li><a href="#" id="trigger">Select Product <i class="arrow"></i></a></li>[wpb_childpages]</ul>'); ?>
                <!--<div style="max-width: 200px; margin: 0 auto;" class="hide-for-medium">
        <a href="http://www.sleeppittsburgh.com/specials/">
        <img src="http://www.sleeppittsburgh.com/wp-content/uploads/2017/08/SIDEBAR-DEALS-01-e1502207091346.png" style="padding-top: 20px;" alt="Great Mattress Deals in Pittsburgh">
        </a>
        </div>
                -->
                <aside class="product_content column large-9">
                    <article class="column clearfix product_intro">
                    <?php the_content(); ?>
                    </article>

        <?php if (have_rows('product_content')): ?>
            <?php while (have_rows('product_content')) : the_row(); ?>

                                    <?php if (get_row_layout() == 'store_availability'): ?>
                                <article class="column clearfix store_availability">
                                    <h4><?= get_field('availability_header') ?></h4>
                                    <div class="isp">
                                            <?php if (have_rows('available_store_products')): ?>
                                            <div class="row">
                                                <?php $off = 0;
                                                while (has_sub_field('available_store_products')):
                                                    ?>
                                                    <?php
                                                    $image = get_sub_field('asp_imagery');
                                                    if (!empty($image)):
                                                        ?>
                                                                    <?php
                                                                    if ($off % 3 == 0 && $off > 0) {
                                                                        echo '</div><div class="row">';
                                                                    }
                                                                    ?>
                                                        <div class="fpc medium-4 columns">
                                                            <div class="product" style="background: url('<?php echo $image['url']; ?>') no-repeat center center; background-size: contain;">
                                                                <div class="metadata">
                                                            <?php if (get_sub_field('asp_bs')): ?>
                                                                        <span class="label bs">BEST SELLER</span>
                                                            <?php endif; ?>
                                                                </div>
                                                        <?php if (get_sub_field('asp_off')): ?>
                                                                    <span class="label off">$<?= get_sub_field('asp_off') ?><br>off</span>
                                                        <?php endif; ?>
                                                            </div>
                                <?php if (get_sub_field('asp_caption')): ?>
                                                                <p><?php the_sub_field('asp_caption'); ?></p>
                                            <?php endif; ?>
                                                        </div>
                                            <?php $off++; ?>
                                        <?php endif; ?>
                                        <?php endwhile; ?>
                                            </div>
                                        </div>
                                    </article>
                                    <?php endif; ?>
                                <?php endif; ?>

                                <?php if (get_row_layout() == 'showcase_our_products'): ?>
                                <article class="column clearfix product_showcase">
                                    <?php if (have_rows('product_showcase_introduction')): ?>
                                        <?php while (has_sub_field('product_showcase_introduction')): ?>
                                                <?php if (get_sub_field("showcase_section_title")): ?>
                                                <h4><?php the_sub_field("showcase_section_title"); ?></h4>
                                                <?php endif; ?>
                                                <?php if (get_sub_field("showcase_section_paragraph")): ?>
                                                <p><?php the_sub_field("showcase_section_paragraph"); ?></p>
                                                <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                                    <div class="psi row ">
                                                <?php if (have_rows('product_showcase')): ?>
                                                    <?php while (has_sub_field('product_showcase')): ?>
                                                        <?php
                                                        $image = get_sub_field('product_image');
                                                        if (!empty($image)):
                                                            ?>
                                                    <div class="fpc fpc medium-4 columns">
                                                        <a href="<?php the_sub_field("product_page_link"); ?>">
                                                            <div class="ps_imagery" style="background: url('<?php echo $image['url']; ?>') no-repeat center center; background-size: contain;"></div>
                                                            <?php if (get_sub_field('product_name')): ?>
                                                                <p><?php the_sub_field('product_name'); ?></p>
                                                            <?php endif; ?>
                                                        </a>
                                                    </div>
                                                <?php else: ?>
                                                    <?php $upload_dir = wp_upload_dir(); ?>
                                                    <div class="fpc clearfix">
                                                        <a href="<?php the_sub_field("product_page_link"); ?>">
                                                            <div class="ps_imagery" style="background: url('<?php echo $upload_dir['baseurl'] . '/2016/12/wp-image-settings.jpg'; ?>') no-repeat center center; background-size: contain;"></div>
                                <?php if (get_sub_field('product_name')): ?>
                                                                <p><?php the_sub_field('product_name'); ?></p>
                                            <?php endif; ?>
                                                        </a>
                                                    </div>
                                            <?php endif; ?>

                                        <?php endwhile; ?>
                                        </div>
                                    </article>
                                    <!-- <?php endif; ?> -->
                                    <?php endif; ?>

                                <?php if (get_row_layout() == 'content_block'): ?>
                                <article class="column clearfix pContent">
                                <?php if (have_rows('product_cb')): ?>
                                    <?php while (has_sub_field('product_cb')): ?>
                                            <article>
                                        <?php if (get_sub_field('content')): ?>
                                                    <p><?php the_sub_field('content'); ?></p>
                                            <?php endif; ?>
                                            </article>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </article>
                                <?php endif; ?>

                                <?php if (get_row_layout() == 'major_product_brands'): ?>
                                <article class="column clearfix majors_brands">
                                        <?php if (have_rows('mpb_overview')): ?>
                                            <?php while (has_sub_field('mpb_overview')): ?>
                                            <h4>Major Brands</h4>
                                                <?php if (get_sub_field('mpb_paragraph')): ?>
                                                <p><?php the_sub_field('mpb_paragraph'); ?></p>
                                                <?php endif; ?>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    <?php if (have_rows('mpb_logos')): ?>
                                        <div class="brands">
                                    <?php while (has_sub_field('mpb_logos')): ?>
                                        <?php
                                        $image = get_sub_field('mpb_logo');
                                        if (!empty($image)):
                                            ?>
                                                    <div class="brand_logo float-left small-12 medium-6 large-4" style="background: url('<?php echo $image['url']; ?>') no-repeat center center; background-size: contain;"></div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                        </div>
                                        <?php endif; ?>
                                </article>
                                <?php endif; ?>

                                <?php if (get_row_layout() == 'rpfy_block'): ?>
                                <article class="column clearfix rmfy">
                                    <?php if (have_rows('rpfy_fg')): ?>
                        <?php while (has_sub_field('rpfy_fg')): ?>
                            <?php if (get_sub_field('rpfy_wyswig')): ?>
                                                <aside class="snm">
                                                <?php the_sub_field('rpfy_wyswig'); ?>
                                                </aside>
                                            <?php endif; ?>
                                        <?php
                                        $image = get_sub_field('rpfy_imagery');
                                        if (!empty($image)):
                                            ?>
                                                <aside class="snm_imagery rmfy_imagery">
                                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                                                </aside>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                                </article>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                                <?php endif; ?>


        <?php if (is_single('mattresses')): ?>

                        <article class="column rmfy">
                            <aside class="snm">

                                        <?php if (have_rows('rfanm')): ?>
                                        <?php while (has_sub_field('rfanm')): ?>
                                        <div class="mattress-block" id="<?php if (get_sub_field("product_id")): ?><?php the_sub_field("product_id"); ?><?php endif; ?>">
                                        <?php if (get_sub_field("title")): ?>
                                                <div class="pdhc">
                                                    <h4 id="<?php the_sub_field('title_class'); ?>"><?php the_sub_field("title"); ?></h4>
                                                </div>
                    <?php endif; ?>
                    <?php if (get_sub_field("description")): ?>
                                                <div class="pdpc" id="<?php the_sub_field('description_class'); ?>">
                        <?php the_sub_field("description"); ?>
                                                </div>
                    <?php endif; ?>
                                        </div>
                <?php endwhile; ?>
            <?php endif; ?>
                            </aside>
                            <aside class="snm_imagery rmfy_imagery">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="353.8px" height="391.8px" viewBox="0 0 353.8 391.8" style="enable-background:new 0 0 353.8 391.8;" xml:space="preserve"
                                     >
                                <style type="text/css">
                                    .st0{fill:#FFFFFF;stroke:#3A3A49;stroke-width:1.5;stroke-miterlimit:10;}
                                    .st1{fill:#3A3A49;}
                                    .st2{font-family:'OpenSans-Bold', sans-serif !important;}
                                    .st3{font-size:11px;}
                                </style>
                                <defs>
                                <linearGradient id="gradient1" x1="0" y1="0" x2="100%" y2="0"><stop offset="0%" stop-color="#9c2363" stop-opacity="0.85"></stop><stop offset="100%" stop-color="#65318f"></stop></linearGradient>
                                <linearGradient id="gradient2" x1="0" y1="0" x2="0" y2="1" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#gradient1"></linearGradient>
                                </defs>
                                <g>
                                <g>
                                <a href="#" id="king_trigger">
                                    <path class="st0" d="M343.1,367.4H26.5c-5.5,0-10-4.5-10-10V35.2c0-5.5,4.5-10,10-10h316.6c5.5,0,10,4.5,10,10v322.2
                                          C353.1,362.9,348.6,367.4,343.1,367.4z"/>
                                </a>
                                <a href="#" id="queen_trigger">
                                    <path class="st0" d="M57.3,0.8h255.1c5.5,0,10,4.5,10,10V333c0,5.5-4.5,10-10,10H57.3c-5.5,0-10-4.5-10-10V10.8
                                          C47.3,5.2,51.8,0.8,57.3,0.8z"/>
                                </a>
                                <a href="#" id="full_trigger">
                                    <path class="st0" d="M262.3,391.1H38.5c-5.5,0-10-4.5-10-10V80.4c0-5.5,4.5-10,10-10h223.9c5.5,0,10,4.5,10,10V381
                                          C272.3,386.6,267.8,391.1,262.3,391.1z"/>
                                </a>
                                <a href="#" id="twin_trigger">
                                    <path class="st0" d="M12,377.4h159.1c5.5,0,10-4.5,10-10V94.1c0-5.5-4.5-10-10-10H12c-5.5,0-10,4.5-10,10v273.3
                                          C2,372.9,6.5,377.4,12,377.4z"/>
                                </a>
                                </g>
                                <g>
                                <g>
                                <g>
                                <text transform="matrix(1 0 0 1 139.5127 20.2162)" class="st1 st2 st3" id="qst">QUEEN 60” X 80”</text>
                                <text transform="matrix(0 1 -1 0 252.1602 164.2336)" class="st1 st2 st3" id="fst">FULL / DOUBLE 54” X 75”</text>
                                <text transform="matrix(1 0 0 1 50.6565 103.2013)" class="st1 st2 st3" id="tst">TWIN 39” X 75”</text>
                                <g>
                                <text transform="matrix(0 1 -1 0 333.0703 156.8688)" class="st1 st2 st3" id="kst">KING 76” X 80”</text>
                                </g>
                                </g>
                                </g>
                                </g>
                                </g>
                                </svg>
                            </aside>
                        </article>
        <?php endif; ?>
                </aside>
            </section>
        </div>
    <?php endwhile;
endif;
?>
<?php get_footer(); ?>
<?php if (is_single("mattresses")): ?>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/products.js"></script>
<?php endif; ?>
