<?php get_header(); ?>
<div class="row">
	<header role="page-header">
		<h2 class="text-center"><?php the_title(); ?></h2>
		<ul class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></ul>
	</header>
	<section class="clearfix blog_content">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<aside class="column medium-12">
			<?php $image = get_field('featured_post_thumbnail');
			if( !empty($image) ): ?>
				<aside class="column medium-4">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
				</aside>
				<aside class="column medium-8">
					<div class="blog_summary">
						<?php the_content(); ?>
					</div>
				</aside>
			<?php else: ?>
				<aside class="column medium-12">
					<div class="blog_summary">
						<?php the_content(); ?>
					</div>
				</aside>
			<?php endif; ?>
		</aside>
		<?php endwhile; endif; ?>
	</section>
</div>
<?php get_footer(); ?>
