<?php
/* Template Name: Our Specials */
get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row">
	<header role="page-header">
		<h2 class="text-center"><?php the_title(); ?></h2>
		<ul class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></ul>
	</header>
	<section class="clearfix">
		<aside class="column large-12">
			<?php
			$args = array(
		    'post_type' => 'specials',
		   	'posts_per_page' => '-1',
				'order'		=> 'date',
			);
			// get results
			$the_query = new WP_Query( $args );
			// The Loop
			if( $the_query->have_posts() ): ?>
			<section class="specials">
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<div class="column large-6 special">
					<h4><?php the_title(); ?></h4>
					<?php
					$image = get_field('special_flyer');
					if( !empty($image) ): ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endif; ?>
					<?php if(get_field('special_description')): ?>
						<p><?php the_field('special_description'); ?></p>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
			</section>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
		</aside>
	</section>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
