<article class="blog_post clearfix">
<header>
  <h3 class="blog_title"><?php the_title(); ?></h3>
	<?php if ( !is_search() ) get_template_part( 'entry', 'meta' ); ?>
</header>
<?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>
</article>
