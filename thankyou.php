<?php
/* Template Name: Thank You */
get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row">
	<header role="page-header">
		<h2 class="text-center"><?php the_title(); ?></h2>
	</header>
	<section class="clearfix thank-you">
		<aside class="column large-12">
			<?php the_content(); ?>
		</aside>
	</section>
</div>
<?php endwhile; endif; ?>
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/875461483/?label=Zcg2COiI920Q6_a5oQM&amp;guid=ON&amp;script=0"/>
<?php get_footer(); ?>
