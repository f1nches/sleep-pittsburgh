<section class="entry-content">
	<?php $image = get_field('featured_post_thumbnail');
	if( !empty($image) ): ?>
	<div class="featured_thumbnail">
		<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
	</div>
	<?php endif; ?>
<div class="blog_summary">
	<?php the_excerpt(); ?>
	<a href="<?php the_permalink(); ?>" class="read-more">Continue Reading <i>&rarr;</i></a>
</div>
</section>
