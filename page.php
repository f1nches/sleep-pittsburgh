<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row">
	<header role="page-header">
		<h2 class="text-center"><?php the_title(); ?></h2>
		<ul class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></ul>
	</header>
	<section class="clearfix">
		<aside class="column large-12">
			<?php the_content(); ?>
		</aside>
	</section>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
