</main>
<footer class="footer_tagline">
	<?php if(get_field('tagline', 'options')): ?>
		<p><?php the_field('tagline', 'options'); ?></p>
	<?php endif; ?>
</footer>
<footer class="primary_footer">
	<div class="row">
		<div class="column large-3">
			<a class="footer_logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr_e( get_bloginfo( 'name' )); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/logos/sp_white_logo.svg" alt="Sleep Pittsburgh - White Branding Logo">
			</a>
    </div>
		<div class="column large-3">
			<p><?php the_field('address', 'options'); ?></p>
			<p><strong>Phone:</strong> <a href="tel:+1-<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number', 'options'); ?></a></p>
			<ul class="store_hours">
				<?php while(has_sub_field('store_hours', 'option')): ?>
					<li>
						<?php if(get_sub_field('day')): ?>
							<strong><?php the_sub_field('day'); ?>:</strong>
						<?php endif; ?>
						<?php if(get_sub_field('hours_open')): ?>
							<?php the_sub_field('hours_open'); ?>
						<?php endif; ?>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>
		<div class="column large-6">
			<ul class="column large-5 footer_menu">
				<?php wp_nav_menu(array('menu' => 'Footer C1', 'container' => '', 'items_wrap' => '%3$s')); ?>
			</ul>
			<ul class="column large-5 footer_menu">
				<?php wp_nav_menu(array('menu' => 'Footer C2', 'container' => '', 'items_wrap' => '%3$s')); ?>
			</ul>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/min/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/min/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.matchHeight.js"></script>
<?php if($pagename == 'about-us' || 'homepage'){ ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plyr.js"></script>
<script type="text/javascript">
  plyr.setup({
    loadSprite: true,
    iconUrl: '<?php echo get_template_directory_uri(); ?>/images/icons/plyr-play.svg',
  });
</script>
<?php } ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/app.js"></script>
</body>
</html>
