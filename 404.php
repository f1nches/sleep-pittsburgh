<?php get_header(); ?>
<section class="not-found">
	<div class="row">
		<div class="column">
			<article id="post-0" class="post not-found">
			<header role="page-header">
				<h2 class="text-center"><?php _e( 'Not Found', 'blankslate' ); ?></h2>
				<ul class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></ul>
			</header>
			<section class="entry-content">
				<div class="search_form">
					<p><?php _e( 'Nothing found for the requested page. Try a search instead?', 'blankslate' ); ?></p>
					<?php get_search_form(); ?>
				</div>
			</section>
			</article>
		</div>
	</div>
</section>
<?php get_footer(); ?>
