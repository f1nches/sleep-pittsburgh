<?php
/* Template Name: About Us */
get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row">
	<header role="page-header">
		<h2 class="text-center"><?php the_title(); ?></h2>
		<ul class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></ul>
	</header>
	<section class="clearfix about_summary">
		<?php
		$image = get_field('company_about_imagery');
		if( !empty($image) ): ?>
		<aside class="column medium-6 about-column">
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
		</aside>
		<?php endif; ?>
		<aside class="column medium-6 about-column">
			<?php the_content(); ?>
			<?php
			$image = get_field('bbb_certified_logo');
			if( !empty($image) ): ?>
				<div class="bbb_logo">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
				</div>
			<?php endif; ?>
		</aside>
	</section>
	<section class="clearfix about_video">
    <div class="column">
      <?php if(get_field('about_us_video')): ?>
        <div data-video-id="<?php the_field('about_us_video'); ?>" data-type="youtube"></div>
      <?php endif; ?>
			<hr>
    </div>
	</section>
	<section class="clearfix our_commitment">
		<div class="column">
			<?php if(get_field("our_commitment_headline")): ?>
				<header class="oc_headline text-center">
					<h4><?php the_field("our_commitment_headline"); ?></h4>
				</header>
			<?php endif; ?>
			<aside class="column large-6">
			<?php if( have_rows ('our_commitment_numbered_points') ): ?>
			<ul class="oc_key_points">
				<?php while( has_sub_field('our_commitment_numbered_points') ): ?>
				<li>
					<?php if(get_sub_field('headline')): ?>
						<h5><?php the_sub_field('headline'); ?></h5>
					<?php endif; ?>
					<?php if(get_sub_field('description')): ?>
						<p><?php the_sub_field('description'); ?>
					<?php endif; ?>
				</li>
				<?php endwhile; ?>
			</ul>
			<?php endif; ?>
			</aside>
			<?php
			$image = get_field('our_commitment_imagery');
			if( !empty($image) ): ?>
				<aside class="column medium-6">
					<div>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					</div>
				</aside>
			<?php endif; ?>
		</div>
	</section>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
