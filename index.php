<?php get_header(); ?>
<div class="row">
	<header role="page-header">
		<h2 class="text-center">Blog</h2>
		<ul class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></ul>
	</header>
	<section class="clearfix blog_content">
		<aside class="column large-12">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'entry' ); ?>
    <?php endwhile; endif; ?>
		</aside>
	</section>
</div>
<?php get_footer(); ?>
