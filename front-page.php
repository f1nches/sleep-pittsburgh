<?php
/* Template Name: Front Page */
get_header('home');
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div class="row primary_nav">
    <div class="columns small-6 medium-4 large-3">
    <a class="branding" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
      <img src="<?php echo get_template_directory_uri(); ?>/images/logos/sp_blue_logo.svg" alt="Sleep Pittsburgh - Blue Official Logo">
    </a>
                    <div class="phone-number-interior">
                        <div class="float-right"><a href="tel:+1-412-462-7858">412-462-7858</a></div>
                                    </div>
    </div>
    <div class="menu-trigger">
      <a href="#" id="hamburger">
        <span></span>
        <span></span>
        <span></span>
      </a>
    </div>
    <nav>
      <ul class="main_nav">
        <?php wp_nav_menu(array('menu' => 'Primary Nav', 'container' => '', 'items_wrap' => '%3$s')); ?>
      </ul>
    </nav>
  </div>
          <section class="homepage_hero" style="background: url(<?php the_field("home_hero_bg"); ?>) no-repeat center center/cover">
            <div class="row homepage-hero-content-row">
              <div class="column large-9 small-12 small-centered hero-content-box">
                <h1 style="text-align: center;"><?php the_field("home_hero_heading"); ?></h1>
                <div><?php the_field("home_hero_content"); ?></div>
              </div>
            </div>
          </section>
          <section class="sync-box">
              <h3>48 MONTHS SPECIAL FINANCING</h3>
              <p>With minimum financing of $1999 on select brands with approved credit through March 31, 2018.</p>
              <p><a href="<?= site_url() ?>/financing/" class="link">View Details</a></p>
              <!-- <p class="fine-print">*With approved credit, see store for details.</p> -->
          </section>
            <style>

            .column.wu-co:first-of-type div {
              background-position: -40.5em !important;
            }

            .column.wu-co:nth-of-type(2) div {
              background-position: -63em !important;
            }

            .column.wu-co:nth-of-type(3) div {
                background-position: 0 !important;
            }

            .column.wu-co:last-of-type div {
                background-position: -21em !important;
            }
            #videoModal {
              position: fixed;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
              background: none;
              z-index: 4;
              width: 80%;
              border-radius: 2px;
              transition: 0.4s;
              opacity: 0;
              visibility: hidden;
            }
            #page-mask {
              position: fixed;
              z-index: 3;
              background: rgba(0,0,0,0.6);
              top: 0;
              bottom: 0;
              left: 0;
              right: 0;
              opacity: 0;
              visibility: hidden;
            }
            #videoModal.show, #page-mask.show {
              transition: 0.4s;
              opacity: 1;
              visibility: visible;
            }

            /* div.plyr.plyr--youtube {
              padding: 0;
            } */
            .close-reveal-modal {
              position: absolute;
              top: 0;
              right: 5px;
              font-size: 30px;
              line-height: 30px;
              z-index: 9999;
              color: #fff;
            }
            .close-reveal-modal:hover {
              color: #2199e8;
            }
            @media screen and (min-width: 1000px) {
              .close-reveal-modal {
                position: absolute;
                top: 0;
                right: 5px;
                font-size: 50px;
                line-height: 30px;
              }
            }
            #videoModalTitle {
              color: #2b388f;
              padding-left: 5px;
            }
            </style>

            <section class="clearfix why_us">
                <div class="row">
                    <article class="clearfix">
                        <?php if (have_rows('why_us_callouts')): ?>
                            <?php while (has_sub_field('why_us_callouts')): ?>
                                <div class="column large-3 wu-co">
                                    <?php
                                    $image = get_sub_field('icon');
                                    if (!empty($image)):
                                        ?>
                                        <div style="background: url(<?php echo $image['url']; ?>) no-repeat; background-size: cover; ?>"></div>
                                    <?php endif; ?>
                                    <?php if (get_sub_field('icon_tagline')): ?>
                                        <p><?php the_sub_field('icon_tagline'); ?></p>
                                    <?php endif; ?>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </article>
                </div>
            </section>
        <div class="row">
            <section class="clearfix introduction">
                <?php if (have_rows('homepage_introduction')): ?>
                    <?php while (has_sub_field('homepage_introduction')): ?>
                        <?php if (get_sub_field("headline")): ?>
                            <h1><?php the_sub_field("headline"); ?></h1>
                        <?php endif; ?>
                        <?php if (get_sub_field("description")): ?>
                            <p><?php the_sub_field("description"); ?></p>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </section>
            <section class="clearfix featured_deals">
                <div class="row deals-row">
                    <?php if (have_rows('featured_special')): ?>
                        <?php while (has_sub_field('featured_special')): ?>
                            <?php
                            $image = get_sub_field('featured_special_flyer');
                            if (!empty($image)):
                                ?>
                                <div class="small-12 large-4 columns">
                                  <div>
                                    <?php if (get_sub_field("featured_special_flyer_link")): ?>
                                        <a href="<?php the_sub_field("featured_special_flyer_link"); ?>" target="_blank">
                                          <img style="width: 100%; height: 100%;" src="<?= the_sub_field('featured_special_flyer'); ?>"/>
                                        </a>
                                    <?php endif; ?>
                                  </div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php if (have_rows('featured_deal')): ?>
                        <?php while (has_sub_field('featured_deal')): ?>
                            <?php
                            $image = get_sub_field('imagery');
                            if (!empty($image)):
                                ?>
                                <div class="small-12 large-4 columns">
                                  <div>
                                    <?php if (get_sub_field("page_link")): ?>
                                        <a href="<?php the_sub_field("page_link"); ?>" target="_blank">
                                          <img style="width: 100%; height: 100%;" src="<?= the_sub_field('imagery'); ?>"/>
                                        </a>
                                    <?php endif; ?>
                                  </div>
                                </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php if (have_rows('featured_deal_third')): ?>
                    <?php while (has_sub_field('featured_deal_third')): ?>
                        <?php
                        $image = get_sub_field('third_deal_image');
                        if (!empty($image)):
                            ?>
                            <div class="small-12 large-4 columns">
                             <div>
                                <?php if (get_sub_field("third_deal_link")): ?>
                                    <a href="#" data-reveal-id="videoModal" class="radius button">
                                      <img style="width: 100%; height: 100%;" src="<?= the_sub_field('third_deal_image'); ?>"/>
                                    </a>
                                    <div id="page-mask"></div>
                                    <div id="videoModal" class="reveal-modal large" data-reveal aria-labelledby="videoModalTitle" aria-hidden="true" role="dialog">
                                      <!-- <h2 id="videoModalTitle">Why Sleep Pittsburgh?</h2> -->
                                      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                                      <div class="flex-video widescreen vimeo">
                                          <div data-video-id="<?php the_field('about_us_video', 66); ?>" data-type="youtube"></div>
                                      </div>
                                    </div>
                                <?php endif; ?>
                              </div>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>

        </div>
        </section>
        <section class="featured_products">
            <div class="row">
                <div class="heading">
                    <h3>Featured Products</h3>
                </div>
                <?php if (have_rows('featured_products')): ?>
                    <?php while (has_sub_field('featured_products')): ?>
                        <div class="featured_product">
                            <?php if (get_sub_field('product_page_link')): ?>
                                <a href="<?php the_sub_field('product_page_link'); ?>">
                                    <?php
                                    $image = get_sub_field('product_imagery');
                                    if (!empty($image)):
                                        ?>
                                        <div class="image" style="background-image:url(<?php echo $image['url']; ?>);"></div>
                                    <?php endif; ?>
                                    <?php if (get_sub_field('product_title')): ?>
                                        <medium><?php the_sub_field('product_title'); ?></medium>
                                    <?php endif; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </section>
        <section class="testimonials">
            <div class="row">
                <hr class="top">
                <div class="owl-carousel">
                    <?php if (have_rows('testimonials')): ?>
                        <?php while (has_sub_field('testimonials')): ?>
                            <div>
                                <blockquote>
                                    <?php if (get_sub_field('testimonial')): ?>
                                        <p><?php the_sub_field('testimonial'); ?></p>
                                    <?php endif; ?>
                                    <div class="client">
                                        <?php if (get_sub_field('client')): ?>
                                            - <span><?php the_sub_field('client'); ?>,</span>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('location')): ?>
                                            <span><?php the_sub_field('location'); ?></span>
                                        <?php endif; ?>
                                    </div>
                                </blockquote>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <hr class="bottom">
            </div>
        </section>
        </div>
        <section class="clearfix gmap">
            <?php
            $location = get_field('homepage_google_map');
            if (!empty($location)):
                ?>
                <article class="home_map">
                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"><p>
                            <strong>Sleep Pittsburgh</strong><br />
                            401 East 8th Avenue
                            Homestead, PA 15120
                            <br><a target="_blank" href="https://www.google.com/maps/dir/''/401+E+8th+Ave,+Homestead,+PA+15120/@40.4089831,-79.9774316,12z/data=!4m8!4m7!1m0!1m5!1m1!1s0x8834ee3b2f88d11f:0xbee9dbffbae7ab45!2m2!1d-79.9073917!2d40.4090042">Get Directions</a>
                        </p></div>
                </article>
            <?php endif; ?>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZaYdFcbQbQYZV7kaiLKX28tOz_8-LtcM"></script>

            <script>
              jQuery('.close-reveal-modal').click(function() {
                jQuery('#videoModal').removeClass('show');
                jQuery('#page-mask').removeClass('show');
                plyr.get().forEach(function(instance) {
                  instance.pause();
                });
              });
              jQuery('a[data-reveal-id]').click(function(e) {
                e.preventDefault();
                jQuery('#videoModal').addClass('show');
                jQuery('#page-mask').addClass('show');
              });

              // jQuery('body').click(function(event) {
              //   if (!jQuery(event.target).closest("#videoModal,.close-reveal-modal").length) {
              //     jQuery("#videoModal").removeClass('show');
              //     jQuery("#page-mask").removeClass('show');
              //   }
              // });
            </script>

            <script type="text/javascript">
                window.onload = function () {
                    (function ($) {
                        console.log('TESTING MAP LOAD');
                        /*************************************
                         Render map in jQuery element
                         *************************************/
                        function new_map($el) {
                            // Marker variable
                            var $markers = $el.find('.marker');
                            // Marker variable argument(s)
                            var args = {
                                zoom: 16,
                                center: new google.maps.LatLng(0, 0),
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                scrollwheel: false,
                                disableDefaultUI: true,
                                disableDoubleClickZoom: true
                            };
                            // Create google map
                            var map = new google.maps.Map($el[0], args);
                            // Add marker reference
                            map.markers = [];
                            // Add marker to map
                            $markers.each(function () {
                                add_marker($(this), map);
                            });
                            // Center google map
                            center_map(map);
                            // Return map
                            return map;
                        }
                        /*************************************
                         Adds Marker to Google Map
                         *************************************/
                        function add_marker($marker, map) {
                            // Latitude & Longitude Variable(s)
                            var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
                            // Create Marker
                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map,
                                icon: {
                                    url: "<?php echo get_template_directory_uri(); ?>/images/icons/blue-star.svg",
                                }
                            });
                            // Add marker to array
                            map.markers.push(marker);
                            // If marker has HTML, add to info window.
                            if ($marker.html()) {
                                // create info window
                                var infowindow = new google.maps.InfoWindow({
                                    content: $marker.html()
                                });
                                // show info window when marker is clicked
                                google.maps.event.addListener(marker, 'click', function () {
                                    infowindow.open(map, marker);
                                });
                            }
                        }
                        /*************************************
                         Center map and show marker
                         *************************************/
                        function center_map(map) {
                            // Longitude & Latitude Bounds Variable
                            var bounds = new google.maps.LatLngBounds();
                            // loop through all markers and create bounds
                            $.each(map.markers, function (i, marker) {
                                var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
                                bounds.extend(latlng);
                            });
                            // Only 1 Marker [Centered]
                            if (map.markers.length == 1) {
                                map.setCenter(bounds.getCenter());
                                map.setZoom(14);
                            }
                            // Fit to map container bounds
                            else {
                                map.fitBounds(bounds);
                            }
                        }
                        /*************************************
                         Renders new map on document.ready
                         *************************************/
                        // Map null variable
                        var map = null;
                        $(document).ready(function () {
                            $('.home_map').each(function () {
                                map = new_map($(this));
                            });
                        });
                    })(jQuery);

                    var height = $('.fdp').first().height();

                    $('.no-credit-needed').height(height);

                    $(window).resize(function () {
                        var height = $('.fdp').first().height();

                        $('.no-credit-needed').height(height);
                    });

                }

            </script>
            <article class="location_info">
                <h3>Sleep Pittsburgh</h3>
                <ul class="location">
                    <li class="first">
                       <a href="http://maps.apple.com/maps?q=401+E+8th+Ave,+Homestead,+PA+15120">
                         <?php the_field('address', 'options'); ?>
                       </a>
                    </li>
                    <li class="second"><strong>Phone:</strong> <a href="tel:+1-<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number', 'options'); ?></a></li>
                    <li class="third store_hours">
                        <?php while (has_sub_field('store_hours', 'option')): ?>
                            <?php if (get_sub_field('day')): ?>
                                <strong><?php the_sub_field('day'); ?>:</strong>
                            <?php endif; ?>
                            <?php if (get_sub_field('hours_open')): ?>
                                <?php the_sub_field('hours_open'); ?><br />
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </li>
                </ul>
            </article>
        </section>
        <?php
    endwhile;
endif;
?>
<?php get_footer(); ?>
