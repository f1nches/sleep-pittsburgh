<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="row">
            <section class="financial-box">
                <h3>48 MONTHS SPECIAL FINANCING</h3>
                <p>with minimum financing of $1999 on select brands with approved credit through March 31, 2018. Ask for details.</p>
            </section>
        </div>
        <div class="row special-offers">
            <div class="container">
                <div class="medium-4 medium-offset-2 columns">
                    <h4>Special Financing Offers</h4>
                    <h5>48 MONTHS SPECIAL FINANCING*</h5>
                    <p>with minimum financing of $1999 on select brands through March 31, 2018. Subject to credit approval. Ask for details. </p>
                    <h5>12 MONTHS SPECIAL FINANCING*</h5>
                    <p>On any purchase $399 or greater. Subject to credit approval. Ask for details.</p>
                </div>

                <div class="medium-4 columns">
                    <h4>Features</h4>
                    <ul>
                        <li>Convenient monthly payments</li>
                        <li>Easy application</li>
                        <li>Competitive APR</li>
                        <li>Fast credit decisions</li>
                    </ul>
                    <p><small>*Subject to credit approval</small></p>
                </div>
                <div class="medium-12 columns" style="margin-top: 25px; margin-bottom: 25px;">
                    <h3 style="font-weight: bold; text-align: center;">Call for details</h3>
                    <h3 style="font-weight: bold; text-align: center;"><a href="tel:1-412-462-7858">412-462-7858</a></h3>
                </div>
            </div>

        </div>
        <div class="row">
            <section class="financial-box">
                <h3 class="less-than-perfect">Less than Perfect Credit?</h3>
                <p>Sleep Pittsburgh offers 90 days same as cash financing for consumers with less than perfect credit.</p>
                <p><strong>NOTE: The West Creek Financial account has different rates, fees, and terms than those
                        advertised above.</strong></p>
                <p><a target="_blank" href="https://dealer.westcreekfin.com/ApplicationForm/IntroScreen?publicStoreId=449a1e8e-fd9e-4e74-a4a5-1e6b45f0a4b1" class="link">Learn More & Apply</a></p>
            </section>
        </div>
        <?php
    endwhile;
endif;
?>

<?php get_footer(); ?>
